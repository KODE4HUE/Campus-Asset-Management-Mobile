(function () {

    var authenticationFactory = function ($http, $q, dataFactory, localStorageFactory) {

        var factory = {};

        factory.authenticationData = false;
        factory.currentUser = false;

        /**
        * 
        * @params accepts the user crendentials (username and password) then stores then authentication
        * token and current user data to local/sesssion storage
        * returns a $q promise
        */
        factory.login = function(credentials)
        {

            var authenticationData ={};
            var currentUser = {};

            var persistAuthenticationData = function(response)
            {
               authenticationData['accessToken'] = response.data.data.token;

               localStorageFactory.setData('authenticationData', angular.toJson(authenticationData));
               factory.authenticationData = authenticationData;
               
               //return $http.get('http://0.0.0.0:8282/api/auth/user');
               return dataFactory.getRecord('auth/logged-in-user');
            };

            var setCurrentUserProfile = function (response)
            {
                var deferred = $q.defer();

                factory.currentUser = response.data.data;
                
                deferred.resolve();
                return deferred.promise;
            };

            var persistCurrentUserProfile  = function()
            {
              
               var deferred = $q.defer();

               localStorageFactory.setData('currentUser', angular.toJson(factory.currentUser));

               if(localStorageFactory.itemExistInStorage('currentUser'))
               {
                    deferred.resolve();
               }
               else
               {
                    deferred.reject();
               }
                
               return deferred.promise;
            };

            var loginFailHandler = function(error)
            {

                return $q.reject(error);   
            };

            return dataFactory.createRecord('auth/token', credentials )
                .then(persistAuthenticationData)
                 .then(setCurrentUserProfile)
                .then(persistCurrentUserProfile)
                .catch(loginFailHandler)

        };

        /**
        * Prompts a user to reset their password
        *
        */
        factory.resetPassword = function(passwordData)
        {
            var deferred = $q.defer();  

            // post request to reset a staff's password
            dataFactory.createRecord('auth/reset-password', passwordData )
                .then(storeAuthenticationInfo , loginFailHandler);

            return deferred.promise;
        };

        /**
        * Determines whether a user is authenticated
        * @params
        * returns true if user is authentication or false if not
        */

        factory.isAuthenticated = function ()
        {
            var isAuthenticated = false;

            if (factory.authenticationData && factory.currentUser ) 
            {
               isAuthenticated = true;
            } else
            {
               // check if the user information is in storage (session/localStorage)
               var currentUserFromStorage =localStorageFactory.getData('currentUser');
               var authenticationDataFromStorage = localStorageFactory.getData('authenticationData');
               
               if( currentUserFromStorage && authenticationDataFromStorage)
               {
                   // then update factory current user and authenticationData properties
                   factory.currentUser = angular.fromJson(currentUserFromStorage);
                   factory.authenticationData = angular.fromJson(authenticationDataFromStorage);
                   
                   isAuthenticated = true;
                   
               }
            }
            
            return isAuthenticated;

        };

        /**
        * Deletes user persistent from localstorage/session storage
        * @params
        * return 
        */
        factory.logout = function () 
        {
            
            localStorageFactory.clearData('authenticationData');
            localStorageFactory.clearData('currentUser');   
            factory.authenticationData = false;
            factory.currentUser = false;

        };

        return factory;
    };

    authenticationFactory.$inject = ['$http', '$q', 'dataFactory' , 'localStorageFactory'];

    angular.module('assetMgntApp').factory('authenticationFactory', authenticationFactory);
    
}());


