(function() {

    var localStorageFactory = function($window, $rootScope) {
        var factory = {};
        factory.setData = function(key,val) {
            $window.localStorage && $window.localStorage.setItem(key, val);
        };

        factory.getData = function(key) {
            return $window.localStorage && $window.localStorage.getItem(key);
        };

        factory.itemExistInStorage = function(key)
        {
            var itemInStorage = $window.localStorage && $window.localStorage.getItem(key);

            if(!itemInStorage)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        factory.clearData = function(key) {
            $window.localStorage && $window.localStorage.removeItem(key);
        };

        return factory;
    };

    localStorageFactory.$inject = ['$window', '$rootScope'];

    angular.module('assetMgntApp').factory('localStorageFactory', localStorageFactory );
    
})();