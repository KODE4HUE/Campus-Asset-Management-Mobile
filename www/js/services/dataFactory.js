(function() 
{

	var dataFactory = function ($http) 
	{


        var factory 		= [];
        var baseUrl 		= 'http://localhost:8282/api';
        var baseUrlRemote	= '';

        factory.getAllRecords = function(datatype, onSuccess, onError) 
        {
            var url = baseUrl + "/" + datatype;
            console.log("GET: " + url);
            return $http.get(url);
            //.success(onSuccess).error(onError);
        };

        factory.getRecord = function(datatype, onSuccess, onError)
        {
            var url = baseUrl + "/" + datatype;
            console.log("GET: " + url);
            return $http.get(url);
        };

        factory.createRecord = function(datatype, data) 
        {
        	
            var url = baseUrl + "/" + datatype;
            console.log("POST: " + url);
            return $http.post(url, data);
        };

        factory.updateRecord = function(datatype, data) 
        {
        	// add laravel method variable to simulate put
        	data['_method'] = "PUT";

            var url = baseUrl + "/" + datatype;
            console.log("PUT: " + url);
            return $http.post(url, data);
        }

        factory.archiveRecord = function(datatype, id) 
        {
        	var data = {};

        	// add laravel method variable to simulate put
        	data['_method'] = "DELETE";
        	data['id'] = id;

            var url = baseUrl + "/" + datatype + "/" + id;
            console.log("DELETE: " + url);
           return $http.post(url, data);
        }

        return factory;
    }


	dataFactory.$inject = ['$http'];

    angular.module('assetMgntApp').factory('dataFactory', dataFactory);

}());