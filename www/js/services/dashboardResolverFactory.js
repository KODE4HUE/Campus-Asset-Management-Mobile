(function() {


    var dashboardResolverFactory = function()
    {
        var factory = {};

        factory.resolveDefaultDashboard = function(currentUserRoles) {
            //var match = userRoles.split(',');

            var defaultDashboard = null;

            var size  = currentUserRoles.length;


            currentUserRoles.every(function(role, index, array)
            {
                console.log(role);

                if(role['role_name'] == "Administrator")
                {
                    defaultDashboard = 'app.admin-dashboard';
                    return false; // admin is the highest role thus break
                }


                if(role['role_name'] == "Accounts Personnel")
                {
                    console.log('accounts');
                    defaultDashboard = 'app.accounts-dashboard';
                    return false; // admin is the highest role thus break
                }


                if(role['role_name'] == "Security Personnel")
                {
                    console.log('')
                    defaultDashboard = 'app.security-dashboard';
                    return false; // admin is the highest role thus break
                }
                
            });

            return defaultDashboard;
        };

        factory.resolveDashboardLinks = function(userRoles) {
            var match = userRoles.split(',');
            var dashboardLinks = {};
            var index = 0;

            for (var a in match) {
                var role = match[a];
                var link = {};

                if (role == "Administrator") {
                    link = {
                        'name': 'Administrator',
                        'state': 'home.admin'
                    };

                    dashboardLinks[index] = link;
                }

                if (role == 'Security Personnel') {
                    //defaultDashboard = 'home/security';
                    link = {
                        'name': 'Security',
                        'state': 'home.security'
                    };

                    dashboardLinks[index] = link;
                }

                if (role == 'Accounts Personnel') {
                    link = {
                        'name': 'Accounts',
                        'state': 'home.accounts'
                    };

                    dashboardLinks[index] = link;
                }

                index++;
            }

            return dashboardLinks;
        };

        return factory;
    };


    dashboardResolverFactory.$inject = [];
    angular.module('assetMgntApp').factory('dashboardResolverFactory', dashboardResolverFactory);
    
}());