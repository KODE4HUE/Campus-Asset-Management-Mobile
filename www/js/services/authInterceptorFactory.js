(function() {

    'use strict';

    var authInterceptorFactory = function ($rootScope, $q, $location, localStorageFactory)
    {
        var factory = {};

         var _request = function(config) {

           config.headers = config.headers || {};


           var authenticationData = angular.fromJson(localStorageFactory.getData('authenticationData'));
        
            if (authenticationData) 
            {
                config.headers.Authorization = 'Bearer ' + authenticationData['accessToken'];
            }

            return config;
        };

        var _responseError = function(rejection) {
            if (rejection.status === 401) {
                $location.path('/login');
            }
            return $q.reject(rejection);
        };

        factory.request = _request;
        factory.responseError = _responseError;

        return factory;

    };

    authInterceptorFactory.$inject = ['$rootScope', '$q', '$location','localStorageFactory'];

    angular.module('assetMgntApp').factory('authInterceptorFactory', authInterceptorFactory);

}());