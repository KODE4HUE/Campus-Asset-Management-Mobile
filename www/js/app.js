// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('assetMgntApp', ['ionic', 'ngMessages'])

.run(function($ionicPlatform, $state, authenticationFactory, dashboardResolverFactory) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    // if the user is not authenticated  -- require login
     if (!authenticationFactory.isAuthenticated()) 
     {
        $state.go("login"); 
     }
     else
     {
        // otherwise go to dashboard based on role       
        var navigateToState = dashboardResolverFactory.resolveDefaultDashboard(authenticationFactory['currentUser']['roles']);

        console.log(navigateToState);
        $state.go(navigateToState);
     }
  });

})

.config(function($stateProvider, $urlRouterProvider, $httpProvider) {
  $stateProvider


  .state('login', {
    url: "/login",
    cache: false,
    views:{
      'appContent': {
          templateUrl: "templates/authentication/login.html",
          controller: 'loginController'
      }
    }
  })

  .state('app', {
    url: "/app",
    abstract: true,
    views:{
      'appContent': {
           templateUrl: "templates/app.html",
           controller: 'appController'
      }
    }
   
  })

   .state('app.admin-dashboard', {
     parent: 'app',
     url: "/admin-dashboard",
     cache: false,
     views: {
       'dashboardContent': {
         templateUrl: "templates/dashboards/admin-dashboard.html",
          controller: 'adminDashboardController'
       }
     }
   })

   .state('app.browse', {
     url: "/browse",
     views: {
       'menuContent': {
         templateUrl: "templates/browse.html"
       }
     }
   });
   
  //   .state('app.playlists', {
  //     url: "/playlists",
  //     views: {
  //       'menuContent': {
  //         templateUrl: "templates/playlists.html",
  //         controller: 'PlaylistsCtrl'
  //       }
  //     }
  //   })

  // .state('app.single', {
  //   url: "/playlists/:playlistId",
  //   views: {
  //     'menuContent': {
  //       templateUrl: "templates/playlist.html",
  //       controller: 'PlaylistCtrl'
  //     }
  //   }
  // });
  
  /**
   * if none of the above states are matched, use the following as fallback
   * - first if the user is not logged in, then go to the login page
   * -if the user is logged in then check there role and fallback to dashboard
   *   matching the user's role -e.g Admin Dashboard for admin role
   */ 
   
   
 //$urlRouterProvider.otherwise('/app/home');
  
     /**
     * Sets authentication header for each request while a user is authenticated
     */
    $httpProvider.interceptors.push('authInterceptorFactory');
});
