(function () {

	var appController = function($scope, $state, authenticationFactory)
	{
		var factory ={};

		alert('in app controller');
		
		// check if the user is authorized each time they try to go to the app routes
		if(!authenticationFactory.isAuthenticated())
		{
			console.log('from app controller');
			$state.go('login'); // if the user is not authorized to use the app, redirect them to  login
		}

		$scope.logout = function()
		{
			authenticationFactory.logout()
			$state.go('login');
		};

		return factory;

	};

	appController.$inject = ['$scope', '$state',  'authenticationFactory']; // fo handle minification issues

    angular.module('assetMgntApp').controller('appController', appController);

})();