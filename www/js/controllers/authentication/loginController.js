(function() 
{
	var loginController = function($scope, $state, authenticationFactory, dashboardResolverFactory)
	{
		//alert('in login controller');
		
		

		$scope.credentials= {
			'username': '',
			'password': ''
		};
		
		$scope.serverErrorMessage = false;
		$scope.invalidCredentialsErrorMessage = false;
		
		var checkForUserFirstLogin = function(response)
		{
			// retrieve login information from localStorageFactory
           if(authenticationFactory['currentUser']['first_login'] == 1)
           {
              $state.go('reset-password'); // user must reset password
           }
           else
           {

               // use the dashboard resolver to determine the state that the user should be navigated to
               var navigateToState = dashboardResolverFactory.resolveDefaultDashboard(authenticationFactory['currentUser']['roles']);
    			console.log(navigateToState);
                $state.transitionTo(navigateToState);

           }	
		};
		
		var showLoginFail = function (error) {
			console.log(error);
			
			// check if user was unauthorized
			if(error['status'] == '401')
			{
				// display invalid credentials error
				$scope.invalidCredentialsErrorMessage = error.data['error-messages']['server-error'][0];
			}
			else{
				// handle server error
				
			}
			
			//determine the error type
		};

		$scope.login = function()
		{
			$scope.errors = {}; // clean up validation errors
			$scope.invalidCredentialsErrorMessage = false;
			
			alert('clicked login');
			
			var promise = authenticationFactory.login($scope.credentials);
            promise.then(checkForUserFirstLogin, showLoginFail);

		};

	};

	loginController.$inject = ['$scope', '$state', 'authenticationFactory', 'dashboardResolverFactory']; // fo handle minification issues

	angular.module('assetMgntApp').controller('loginController', loginController);

}());